// ТЕОРІЯ
/*
1. Метод forEach з допомогою callBack функції перебирає всі елементи масиву без повернення нового масиву.

2. Методами які мутують массив являються (pop, push, shift, unshift, splice).
Методи які не мутують массив (slice, concat, map, filter, reduce, sort, reverse).


Методами які мутують массив являються (pop, push, shift, unshift, splice, reverse).
Методи які не мутують массив (slice, concat, map, filter, reduce, sort, reverse).

3. За допомогою оператора typeof, або з допомогою метода Array.isArray()

4. Метод forEach() впливає на оригінальний масив і змінює його, а метод map() повертає зовсім новий масив, залишаючи вихідний масив незмінним. */


// ПРАКТИКА

// 1.
let strArray = ["travel", "hello", "eat", "ski", "lift"];
let strTotal = strArray.filter(str => str.length > 3);
console.log(strTotal);

// 2.

const person = [
    {name: "Іван", age: 25, sex: "чоловіча"},
    {name: "Анастасія", age: 18, sex: "жіноча"},
    {name: "Євген", age: 41, sex: "чоловіча"},
    {name: "Марія", age: 21, sex: "жіноча"}
]

let resultTwo = person.filter((key) => key.sex === "чоловіча");

console.log(resultTwo);

// 3.


function filterBy(arr, dataType) {
    return arr.filter(arrItem => typeof arrItem !== dataType);
};
console.log(filterBy(['hello', 'world', 23, '23', null], "string"));



